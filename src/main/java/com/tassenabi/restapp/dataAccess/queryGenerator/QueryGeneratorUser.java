package com.tassenabi.restapp.dataAccess.queryGenerator;

public abstract class QueryGeneratorUser {

    public static final String TABLENAME = "TBL_USER";
    public static final String COLUMN1 = "PK_id";
    public static final String COLUMN2 = "TXT_userName";
    public static final String WHERE = " WHERE ";
    public static final String QUOTATIONMARKS = "'";

}
